﻿label alt_day1_me_7dl_start:
    call alt_day1_me_7dl_vars
    $ routetag = "final"
    $ persistent.sprite_time = "day"
    $ day_time()
    $ alt_chapter(1, u"Семён. Пробуждение")
    call alt_day1_me_7dl_bus_start
    pause(1)
    $ alt_chapter(1, u"Семён. Экскурсия")
    call alt_day1_me_7dl_meeting
    pause(1)
    $ alt_save_name(1, u"Семён. 1988")
    call alt_day1_me_7dl_memory1
    pause(1)
    $ alt_save_name(1, u"Семён. Роковая встреча")
    call alt_day1_me_7dl_lena
    pause(1)
    $ persistent.sprite_time = "sunset"
    $ sunset_time()
    $ alt_chapter(1, u"Семён. Ужин")
    call alt_day1_me_7dl_supper
    pause(1)
    $ alt_save_name(1, u"Семён. Музыкальный клуб")
    call alt_day1_me_7dl_music_club
    pause(1)
    $ alt_save_name(1, u"Семён. 1988")
    call alt_day1_me_7dl_memory2
    pause(1)
    $ alt_save_name(1, u"Семён. Отбой")
    call alt_day1_me_7dl_sleep
    pause(1)
    jump alt_day2_me_7dl_start

label alt_day2_me_7dl_start:
    call alt_day2_me_7dl_vars
    $ persistent.sprite_time = "sunset"
    $ sunset_time()
    $ alt_chapter(2, u"Семён. Утро")
    call alt_day2_me_7dl_begin
    pause(1)
    $ persistent.sprite_time = "day"
    $ day_time()
    $ alt_save_name(2, u"Семён. 1987")
    call alt_day2_me_7dl_memory1
    pause(1)
    $ alt_save_name(2, u"Семён. Утро")
    call alt_day2_me_7dl_begin_continue
    pause(1)
    $ alt_chapter(2, u"Семён. Завтрак")
    call alt_day2_me_7dl_breakfast
    pause(1)
    $ alt_save_name(2, u"Семён. Обход лагеря")
    call alt_day2_me_7dl_bypass
    pause(1)
    $ alt_save_name(2, u"Семён. 1987")
    call alt_day2_me_7dl_memory2
    pause(1)
    $ alt_save_name(2, u"Семён. Обход лагеря")
    call alt_day2_me_7dl_bypass_continue
    pause(1)
    $ alt_chapter(2, u"Семён. Обед")
    call alt_day2_me_7dl_dinner
    pause(1)
    $ alt_save_name(2, u"Семён. Тихий час")
    call alt_day2_me_7dl_silent_hour
    pause(1)
    $ alt_save_name(2, u"Семён. Полдник")
    call alt_day2_me_7dl_lunch
    pause(1)
    $ alt_chapter(2, u"Семён. Пляжный \nэпизод")
    call alt_day2_me_7dl_beach
    pause(1)
    $ persistent.sprite_time = "sunset"
    $ sunset_time()
    $ alt_chapter(2, u"Семён. Ужин")
    call alt_day2_me_7dl_supper
    pause(1)
    $ alt_save_name(2, u"Семён. 1986")
    call alt_day2_me_7dl_memory3
    pause(1)
    $ alt_save_name(2, u"Семён. Ужин")
    call alt_day2_me_7dl_supper_continue
    pause(1)
    $ alt_chapter(2, u"Семён. Тяжёлый разговор")
    call alt_day2_me_7dl_mt_talk
    pause(1)
    call alt_day2_me_7dl_dv_talk
    pause(1)
    $ persistent.sprite_time = "night"
    $ night_time()
    if alt_day2_me_7dl_us_talk:
        $ alt_save_name(2, u"Семён. Цена")
        call alt_day2_me_7dl_us_trace
    else:
        $ alt_save_name(2, u"Семён. Обратно в лагерь")
        call alt_day2_me_7dl_camp
    pause(1)
    $ alt_save_name(2, u"Семён. Отбой")
    call alt_day2_me_7dl_sleep
    pause(3)
    jump alt_day3_me_7dl_start
    return

label alt_day3_me_7dl_start:
    call alt_day3_me_7dl_vars
    $ persistent.sprite_time = "sunset"
    $ sunset_time()
    $ alt_chapter(3, u"Семён. Утро")
    call alt_day3_me_7dl_begin
    pause(1)
    $ alt_save_name(3, u"Семён. 1986")
    call alt_day3_me_7dl_memory1
    pause(1)
    $ persistent.sprite_time = "day"
    $ day_time()
    $ alt_save_name(3, u"Семён. Утро")
    call alt_day3_me_7dl_begin_continue
    pause(1)
    $ alt_chapter(3, u"Семён. Завтрак")
    call alt_day3_me_7dl_breakfast
    pause(1)
    $ alt_save_name(3, u"Семён. День")
    call alt_day3_me_7dl_day
    pause(1)
    $ alt_chapter(3, u"Семён. Обед")
    call alt_day3_me_7dl_dinner
    pause(1)
    $ alt_save_name(3, u"Семён. Тихий час")
    call alt_day3_me_7dl_silent_hour
    pause(1)
    $ alt_chapter(3, u"Семён. Вечер")
    call alt_day3_me_7dl_evening
    pause(1)
    $ persistent.sprite_time = "sunset"
    $ sunset_time()
    $ alt_save_name(3, u"Семён. Ужин")
    call alt_day3_me_7dl_supper
    pause(1)
    $ persistent.sprite_time = "night"
    $ night_time()
    $ alt_chapter(3, u"Семён. Дискотека")
    call alt_day3_me_7dl_disco
    pause(1)
    $ alt_save_name(3, u"Семён. 1987")
    call alt_day3_me_7dl_memory2
    pause(1)
    $ alt_save_name(3, u"Семён. Дискотека")
    call alt_day3_me_7dl_disco_continue
    pause(1)
    $ alt_save_name(3, u"Семён. Отбой")
    call alt_day3_me_7dl_sleep
    pause(1)

label alt_day4_me_7dl_start:
    call alt_day4_me_7dl_vars
    $ persistent.sprite_time = "sunset"
    $ sunset_time()
    $ alt_chapter(4, u"Семён. Утро")
    call alt_day4_me_7dl_begin
    pause(1)
    $ alt_chapter(4, u"Семён. Завтрак")
    call alt_day4_me_7dl_breakfast
    pause(1)
    $ persistent.sprite_time = "day"
    $ day_time()
    $ alt_save_name(4, u"Семён. День")
    call alt_day4_me_7dl_day
    pause(1)
    $ alt_chapter(4, u"Семён. Обед")
    call alt_day4_me_7dl_dinner
    pause(1)
    $ alt_save_name(4, u"Семён. Тихий час")
    call alt_day4_me_7dl_silent_hour
    pause(1)
    $ alt_save_name(4, u"Семён. Полдник")
    call alt_day4_me_7dl_lunch
    pause(1)
    $ alt_chapter(4, u"Семён. Концерт")
    call alt_day4_me_7dl_concert
    pause(1)

    $ persistent.sprite_time = "sunset"
    $ sunset_time()
    $ alt_save_name(4, u"Семён. Ужин")
    call alt_day4_me_7dl_supper
    pause(1)

    $ alt_chapter(4, u"Семён. Вечер")
    call alt_day4_me_7dl_evening
    pause(1)
    if alt_day4_me_7dl_mi_date:
        $ alt_save_name(4, u"Семён. Цена")
        call alt_day4_me_7dl_mi_treehouse
    else:
        $ alt_save_name(4, u"Семён. Обратно в лагерь")
        call alt_day4_me_7dl_mz_checkers
    pause(1)
    $ alt_save_name(4, u"Семён. Отбой")
    call alt_day4_me_7dl_sleep
    pause(1)
    jump alt_day5_me_7dl_start

label alt_day5_me_7dl_start:
    call alt_day5_me_7dl_vars
    $ persistent.sprite_time = "night"
    $ sunset_time()
    $ alt_chapter(5, u"Семён. Утро")
    call alt_day5_me_7dl_begin
    $ persistent.sprite_time = "sunset"
    pause(1)
    $ alt_save_name(5, u"Семён. Завтрак")
    call alt_day5_me_7dl_breakfast
    pause(1)
    $ alt_chapter(5, u"Семён. Свечка")
    $ persistent.sprite_time = "night"
    call alt_day5_me_7dl_cndl
    pause(1)
    $ persistent.sprite_time = "sunset"
    call alt_day5_me_7dl_dinner
    pause(1)
    $ persistent.sprite_time = "day"
    $ day_time()
    $ alt_save_name(5, u"Семён. День")
    call alt_day5_me_7dl_day
    pause(1)
    $ alt_save_name(5, u"Семён. Шпионские игры")
    call alt_day5_me_7dl_spy_games
    pause(1)
    $ alt_save_name(5, u"Семён. Ужин")
    call alt_day5_me_7dl_supper
    pause(1)
    $ persistent.sprite_time = "sunset"
    $ sunset_time()
    $ alt_chapter(5, u"Семён. Поход")
    call alt_day5_me_7dl_campfire
    pause(1)
    call alt_day5_me_7dl_sleep

    $ renpy.pause(2, hard=True)
    show alt_credits timeskip_dev at truecenter with dissolve2
    $ renpy.pause(4, hard=True)
    with dissolve2
    return

